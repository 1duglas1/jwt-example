import models from '../db';
import jwt from 'jsonwebtoken';
import uuid from 'uuid/v4';

import config from '../config';
const { secretJwt: secret } = config;

function createAccessToken(userId) {
  return jwt.sign({ userId }, secret, { expiresIn: '30s' });
}

function createRefreshToken(userId) {
  return models.RefreshToken.create({
    id: uuid(),
    UserId: userId,
  });
}

function deleteRefreshToken(id) {
  return models.RefreshToken.destroy({ where: { id } });
}

async function updateRefreshToken({ userId, tokenId }) {
  const [refreshToken] = await Promise.all([
    createRefreshToken(userId),
    deleteRefreshToken(tokenId),
  ]);

  return refreshToken;
}

export async function createTokens(userId) {
  const refreshToken = await createRefreshToken(userId);
  const accessToken = createAccessToken(userId);

  return {
    refreshToken: refreshToken.id,
    token: accessToken,
  };
}

export async function updateTokens(refreshTokenId) {
  const oldRefreshToken = await models.RefreshToken.findById(refreshTokenId);
  if (!oldRefreshToken) {
    const error = new Error('Forbidden!');
    error.status = 403;
    throw error;
  }

  const userId = oldRefreshToken.UserId;

  const refreshToken = await updateRefreshToken({ userId, tokenId: refreshTokenId });
  const accessToken = createAccessToken(userId);
  return {
    refreshToken: refreshToken.id,
    token: accessToken,
  };
}

export async function deleteRefreshTokens(userId) {
  return models.RefreshToken.destroy({ where: { UserId: userId } });
}
