import Koa from 'koa';
import appRouter from './routes/index';
import db from './db';

import errorHandler from './middlewares/errorHandler';

const app = new Koa();

app.use(errorHandler);
app.use(appRouter.routes());

db.sequelize.sync().then(() => app.listen(3000));
