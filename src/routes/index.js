import Router from 'koa-router';
import koaBody from 'koa-bodyparser';
const appRouter = new Router();

// routes
import usersRouter from './users';
import authRouter from './auth';

appRouter.use(koaBody());

const routes = {
  '/users': usersRouter,
  '/auth': authRouter,
};

Object.keys(routes).forEach(key => {
  const router = routes[key];
  appRouter.use(key, router.routes(), router.allowedMethods());
});

export default appRouter;
