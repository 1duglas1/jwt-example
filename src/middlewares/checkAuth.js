import jwtMiddleware from 'koa-jwt';
import models from '../db';

import config from '../config';
const { secretJwt: secret } = config;

export default function checkAuth(ctx, next) {
  return jwtMiddleware({ secret, key: 'jwt' })(ctx, async () => {
    const { userId } = ctx.state.jwt;
    const user = await models.User.findById(userId, {
      attributes: { exclude: 'password' },
    });

    ctx.state.user = user;
    await next();
  });
}

// Удобный вариант для проверки на роль в роуте
// К примеру:
// adminRoute.get('/data', checkAuthAndRole('admin'), async ctx => {...}) -> Допускает только админа на урл
// privateRoute.get('/data', checkAuthAndRole(['admin', 'manager']), async ctx => {...}) -> Допускает и менеджера и админа
// profileRoute.get('/', checkAuthAndRole(), async ctx => {...}) -> Допускает всех авторизированных юзеров
const checkAuthAndRole = roles =>
  jwtMiddleware({ secret, key: 'jwt' })(ctx, async () => {
    const { userId } = ctx.state.jwt;
    const user = await models.User.findById(userId, {
      attributes: { exclude: 'password' },
      plain: true,
    });

    if (Array.isArray(roles) && !roles.includes(user.role)) ctx.throw(403);
    if (typeof roles === 'string' && roles !== user.role) ctx.throw(403);

    ctx.state.user = user;
    await next();
  });
