import Router from 'koa-router';
import { createUser } from '../services/user';

const router = new Router();

router.post('/', async ctx => {
  const { login, password } = ctx.request.body;
  await createUser({ login, password });
  ctx.status = 204;
});

export default router;
