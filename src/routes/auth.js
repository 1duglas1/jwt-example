import Router from 'koa-router';
import models from '../db';

import { comparePasswordHash } from '../helpers/password';
import { createTokens, deleteRefreshTokens, updateTokens } from '../services/authToken';

import checkAuth from '../middlewares/checkAuth';

const router = new Router();

router.post('/login', async ctx => {
  const { login, password } = ctx.request.body;

  const user = await models.User.findOne({ where: { login } });
  if (!user) ctx.throw(404, 'User is not found');

  if (!comparePasswordHash(password, user.password)) ctx.throw(403, 'Wrong password');

  ctx.body = await createTokens(user.id);
});

router.post('/refresh', async ctx => {
  const { refreshToken } = ctx.request.body;
  ctx.body = await updateTokens(refreshToken);
});

router.post('/logout', checkAuth, async ctx => {
  await deleteRefreshTokens(ctx.state.user.id);
  ctx.status = 204;
});

export default router;
