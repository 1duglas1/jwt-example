module.exports = (sequelize, DataTypes) => {
  const RefreshToken = sequelize.define('RefreshToken', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      // defaultValue: sequelize.literal('uuid_generate_v4()'),
    },
  });
  RefreshToken.associate = models => {
    RefreshToken.belongsTo(models.User);
  };
  return RefreshToken;
};
