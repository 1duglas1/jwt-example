import models from '../db';
import { createPasswordHash } from '../helpers/password';

export function createUser({ login, password }) {
  return models.User.create({
    login,
    password: createPasswordHash(password),
  });
}
