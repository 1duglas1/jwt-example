import bcryptjs from 'bcryptjs';

export function createPasswordHash(password) {
  return bcryptjs.hashSync(password);
}

export function comparePasswordHash(password, hash) {
  return bcryptjs.compareSync(password, hash);
}
